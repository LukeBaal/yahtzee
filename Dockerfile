FROM python:3.6
WORKDIR /yahtzee
COPY . /yahtzee

RUN pip install -r requirements.txt

CMD ["python", "yahtzee_test.py"]